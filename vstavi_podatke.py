import psycopg2, psycopg2.extensions, psycopg2.extras
psycopg2.extensions.register_type(psycopg2.extensions.UNICODE) # se znebimo problemov s šumniki

import auth_public as AUTH
import re
vzorec_id=r"[0-9]{7}"
vzorec_ocena=r"([0-9]{1}\.[0-9]{1})/[0-9]{2}"
zanri = {1:'action', 2:'adventure', 3:'animation', 4:'biography', 5:'comedy', 6:'crime', 7:'drama', 8:'family', 9:'fantasy',10:'history', 11:'horror', 12:'music', 13:'musical',14:'mystery', 15:'romance',16:'sci_fi', 17:'sport', 18:'thriller', 19:'war', 20:'western'}

conn = psycopg2.connect(database=AUTH.db, host=AUTH.host, user=AUTH.user, password=AUTH.password)
conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT) # onemogočimo transakcije
cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

##Podatke bom vstavljal za vsako tabelo posebej(eno tabelo naenkrat).
dodani_reziserji=set()
cudna_imena=set()

with open("seznamNaslovov.txt","r",encoding="utf-8") as f:
   filmi = f.readlines()
for film in filmi:
    (Id_filma,naslov,leto,režiser,igralci,žanri,ocena,dolžina,povzetek)=tuple(film.strip("\n").split("| "))
    
## Tabela filmi:                    NAREJENO!!!  

##    Id_filma = re.findall(re.compile(vzorec_id),Id_filma)[0]
##    ocena = re.findall(re.compile(vzorec_ocena),ocena)[0]
##    dolžina=dolžina.strip('(),\"\'')
##
##    cur.execute("""INSERT INTO filmi (id, ime, leto_nastanka, dolžina, ocena) VALUES ({0}, {1}, {2}, {3}, {4});""".format(int(Id_filma),"'{0}'".format(naslov),int(leto),int(dolžina),float(ocena)))


# Tabela filmi:                    NAREJENO!!!  

##    Id_filma = re.findall(re.compile(vzorec_id),Id_filma)[0]
##    povzetek = povzetek.strip('\"\',()')
##    
##    try:
##        cur.execute("""UPDATE filmi SET povzetek = \'%s\' WHERE id = %d;"""% (povzetek,int(Id_filma)))
##    except:
##        continue

    
####Tabela igralci:                 NAREJENO!!!
##with open("podatkiIgralci.txt","r",encoding="utf-8") as f:
##    igralci=f.readlines()
##    print(igralci[0].split('|'))
##    for igralec in igralci:
##        podatki=tuple(igralec.split('|'))
##        if len(podatki)==3:
##            (Id,ime,rojstvo)=podatki
##            rojstvo=rojstvo.strip('\n')
##            cur.execute('''INSERT INTO igralci (id, ime, rojstvo, smrt) VALUES ({0}, {1}, {2}, {3});'''.format(int(Id),"'{0}'".format(ime),int(rojstvo),0))
##        else:
##            (Id,ime,rojstvo,smrt)=podatki
##            smrt=smrt.strip('\n')
##            cur.execute('''INSERT INTO igralci (id, ime, rojstvo, smrt) VALUES ({0}, {1}, {2}, {3});'''.format(int(Id),"'{0}'".format(ime),int(rojstvo),int(smrt)))



##                                  NAREJENO!!!
 
####Tabela žanri:
##for key, value in zanri.items():
##    cur.execute('''INSERT INTO žanri (ime) VALUES (\'{0}\');'''.format(value))

##Tabela režiserji:             NAREJENO!!!
##    (Id,ime)=režiser.strip('()\n').split(', ')
##    Id=Id.strip('\'\"')
##    ime=ime.strip('\'\"')
##    if Id=='':
##        continue
##    if '\'' in ime:
##        cudna_imena.add((Id,ime))
##        continue
##    if Id in dodani_reziserji:
##        continue
##    else:
##        cur.execute('''INSERT INTO režiserji (id, ime) VALUES ({0}, \'{1}\');'''.format(int(Id),ime))
##        dodani_reziserji.add(Id)
###    cur.execute('''INSERT INTO režiserji (id, ime) VALUES (0640334, \'Gavin OConnor\');''') #Dodam ga ročno, ker je edina izjema, ki je program ni dodal.

##Tabela nastopajoči:           NAREJENO!!!
##    Id_igralcev=re.findall(re.compile(vzorec_id),igralci)
##    Id_filma = re.findall(re.compile(vzorec_id),Id_filma)[0]
##    for i in Id_igralcev:
##        try:
##            cur.execute('''INSERT INTO nastopajoči (id_filma, id_igralca) VALUES ({0}, {1});'''.format(Id_filma,i))
##        except psycopg2.IntegrityError:
##            continue

##Tabela pripada_žanrom:        NAREJENO!!!
##    Id_filma = re.findall(re.compile(vzorec_id),Id_filma)[0]
##
##    žanri=žanri.strip('()').split(', ')
##    žanri=[žanr.strip(',\'\"') for žanr in žanri]
##    for žanr in žanri:
##        for key in range(1,21):
##            if žanr==zanri[key]:
##                cur.execute('''INSERT INTO pripada_žanrom (id_filma, id_žanra) VALUES ({0}, {1});'''.format(Id_filma,key))

##Tabela režiral:               NAREJENO!!!
##    Id_filma = re.findall(re.compile(vzorec_id),Id_filma)[0]
##    Id_reziser = re.findall(re.compile(vzorec_id),režiser)
##    if Id_reziser==[]:
##        continue
##    else:
##        cur.execute('''INSERT INTO režiral (id_filma, id_režiserja) VALUES ({0}, {1});'''.format(Id_filma,Id_reziser[0]))
    
conn.commit()
cur.close()
conn.close()
