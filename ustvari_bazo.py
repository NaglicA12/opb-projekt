#!/usr/bin/python
# -*- encoding: utf-8 -*-

# uvozimo bottle.py
from bottle import *

# uvozimo ustrezne podatke za povezavo
import auth_public as AUTH

# uvozimo psycopg2
import psycopg2, psycopg2.extensions, psycopg2.extras
psycopg2.extensions.register_type(psycopg2.extensions.UNICODE) # se znebimo problemov s šumniki

# odkomentiraj, če želiš sporočila o napakah
# debug(True)

TABELE = [
"""
CREATE TABLE IF NOT EXISTS filmi (
    id                 int PRIMARY KEY,
    ime                text NOT NULL,
    leto_nastanka      int NOT NULL,
    dolžina            int NOT NULL,
    ocena              real NOT NULL,
    režiser            int DEFAULT=NULL
);
""",

"""
CREATE TABLE IF NOT EXISTS žanri (
    id                 serial PRIMARY KEY,
    ime                text NOT NULL
);
""",

"""
CREATE TABLE IF NOT EXISTS režiserji (
    id                 int PRIMARY KEY,
    ime                text NOT NULL
);
""",

"""
CREATE TABLE IF NOT EXISTS igralci (
    id                 int PRIMARY KEY,
    ime                text NOT NULL,
    rojstvo            int,
    smrt               int
);
""",

"""
CREATE TABLE IF NOT EXISTS nagrade (
    id                 serial PRIMARY KEY,
    ime                text NOT NULL
);
""",

"""
CREATE TABLE IF NOT EXISTS podobni_filmi (
    id1                 int REFERENCES filmi(id),
    id2                 int REFERENCES filmi(id),
    PRIMARY KEY(id1,id2)
);
""",

"""
CREATE TABLE IF NOT EXISTS nastopajoči (
    id_filma                 int REFERENCES filmi(id),
    id_igralca               int REFERENCES igralci(id),
    PRIMARY KEY(id_filma,id_igralca)
);
""",

"""
CREATE TABLE IF NOT EXISTS pripada_žanrom (
    id_filma                 int REFERENCES filmi(id),
    id_žanra                 int REFERENCES žanri(id),
    PRIMARY KEY(id_filma,id_žanra)
);
""",

"""
CREATE TABLE IF NOT EXISTS režiral (
    id_filma                 int REFERENCES filmi(id),
    id_režiserja               int REFERENCES režiserji(id),
    PRIMARY KEY(id_filma,id_režiserja)
);
"""
    ]


######################################################################
# Glavni program

# priklopimo se na bazo

conn = psycopg2.connect(database=AUTH.db, host=AUTH.host, user=AUTH.user, password=AUTH.password)
conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT) # onemogočimo transakcije
cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor) 

# poženemo strežnik na portu 8080, glej http://localhost:8080/
#run(host='localhost', port=8080)

for tabela in TABELE:
    cur.execute(tabela)

conn.commit()
cur.close()
conn.close()
