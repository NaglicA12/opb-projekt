zanri = {1:'action', 2:'adventure', 3:'animation', 4:'biography', 5:'comedy', 6:'crime', 7:'drama', 8:'family', 9:'fantasy',11:'history', 12:'horror', 13:'music', 14:'musical',15:'mystery', 16:'romance',17:'sci_fi', 18:'sport', 19:'thriller', 20:'war', 21:'western'}

vzorecReziser = r"""<span class="credit">
    (Dir: <a href="/name/nm([0-9]+)/">([ a-zA-Z'0-9()\[\]ß?!&%#,;:./\-_*+čšžćđæČĆŽŠÈåĐôçüóûêûîûôôöôéíñàáÉèùÔéâøÆ°ë$ÀéäâêÞóÁïâßô³û·½¡]+)</a>)*"""

vzorecCast =r"""With: (.{300})"""

vzorecIgralec = r"""<a href="/name/nm([0-9]+)/">([ a-zA-Z'0-9()\[\]ß?!&%#,;:./\-_*+čšžćđæČĆŽŠÈåĐôçüóûêûîûôôöôéíñàáÉèùÔéâøÆ°ë$ÀéäâêÞóÁïâßô³û·½¡]+)</a>"""

vzorecGenre = r"""<span class="genre">(.{300})"""

vzorecZanr =r"""<a href="/genre/([a-z_]+)">[A-Za-z\-]+</a>"""

vzorecDolzina = r"""<span class="runtime">([0-9]+) mins.</span>"""

vsiReziserji=set()
vsiIgralci=set()

import urllib.request
import re
import os.path

vsiNaslovi=[]
for key, value in zanri.items():
    with urllib.request.urlopen("""http://www.imdb.com/search/title?genres={0}&sort=user_rating,desc&title_type=feature&num_votes=25000,&pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2406822102&pf_rd_r=0W9S6H3MKATYKT44E50X&pf_rd_s=right-6&pf_rd_t=15506&pf_rd_i=top&ref_=chttp_gnr_{1}""".format(value, key)) as f:
        html=f.read().decode('utf-8')
        vzNaslov = re.compile(r"""<a href="/title/tt([0-9]+)/" title="([ a-zA-Z'0-9()\[\]ß?!&%#,;:./\-_*+čšžćđæČĆŽŠÈåĐôçüóûêûîûôôöôéíñàáÉèùÔéâøÆ°ë$ÀéäâêÞóÁïâßô³û·½¡]+)"><img src=""")
        vzRating = re.compile(r'title="Users rated this ([0-9\./]+) \([0-9,]+ votes\) - click stars to rate">')
        vzReziser = re.compile(vzorecReziser)
        vzSummary = re.compile(r"""<span class="outline">([ a-zA-Z'"0-9()\[\]ß?!&%#,;:./\-_*+čšžćđæČĆŽŠÈåĐôçüóûêûîûôôöôéíñàáÉèùÔéâøÆ°ë$ÀéäâêÞóÁïâßô³û·½¡\s<>=\$«»]+)</span>""")
        vzCast = re.compile(vzorecCast,re.DOTALL)
        vzGenre = re.compile(vzorecGenre, re.DOTALL)
        
        #Vsak naslov je zapisan v obliki (idFilma,naslovFilma (letnica)). Primer: ('0468569', 'The Dark Knight (2008)'). V datoteko so ti trije podatki zapisano ločeno.
        nasloviX =re.findall(vzNaslov,html)
        #Primer: ('9.0/10',)
        ratingX= re.findall(vzRating, html)
        #Zapisan v tuplu, kot vsi ostali podatki. Zaradi tega bo lažje slitati podatke v teh datotekah, ker vejice ne bodo v napoto.
        summaryX1 = re.findall(vzSummary, html)
        # (idReziserja, imeReziserja) Primer: ('0634240', 'Christopher Nolan')
        reziserX = [(i[1],i[2]) for i in re.findall(vzReziser, html)]
        #Tuple tuplov. Vsak igralec ima zraven še svoj id. Primer: (('0000288', 'Christian Bale'), ('0005132', 'Heath Ledger'), ('0001173', 'Aaron Eckhart'))
        castX1 = re.findall(vzCast,html)
        #Tuple žanrov. Primer: ('action', 'adventure', 'crime', 'thriller')
        genreX1 = re.findall(vzGenre, html)
        #seznam dolžin filmov. V datoteke dodajam tuple (dolzina,).
        dolzineX = re.findall(vzorecDolzina, html)


        #Vzorec za summary ni perfekten, zato pri vsakem filmu dobim nekaj značk na koncu. Te odstranim v naslednji zanki. Še vedno, pa so ponekod linki (<a href=....) sredi summaryjev.
        summaryX=[]
        for i in summaryX1:
            I=re.split("</span>",i)
            summaryX.append((I[0],))

        #Pri iskanju regex, ki bi mi dal vse igralce, sem obupal, zato v castX1 zapišem grobe verzije, ki jih nato še prečistim. Za vsak film dobim tuple, ki ima za elemente (idIgralca,imeIgralca).
        castX=[]
        for i in castX1:
            vzIgralec = re.compile(vzorecIgralec)
            castX.append(tuple(re.findall(vzIgralec,i)))
        #print(value, castX)
            
        #Podobno, kot pri igralcih.
        zanriX=[]
        for i in genreX1:
            vzZanr = re.compile(vzorecZanr)
            zanriX.append(tuple(re.findall(vzZanr, i)))
        #print(value,zanriX)
            
###NE RABIM!###       
#V datoteke zapišem podatke po žanrih. Primer:
#'0468569', 'The Dark Knight', '2008', ('0634240', 'Christopher Nolan'), (('0000288', 'Christian Bale'), ('0005132', 'Heath Ledger'), ('0001173', 'Aaron Eckhart')), ('action', 'adventure', 'crime', 'thriller'), ('9.0/10',), ('When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, the caped crusader must come to terms with one of the greatest psychological tests of his ability to fight injustice.',)
#
#        with open("podatki-{0}.txt".format(value),"w",encoding="utf-8") as f:
#            k=1
#            for i in range(50):
#                (naslov,leto)=nasloviX[i][1].split("(")
#                f.write(nasloviX[i][0]+", "+naslov[:len(naslov)-1]+", "+leto[:len(leto)-1]+", "+str(reziserX[i])+", "+str(castX[i])+", "+str(zanriX[i])+", "+str((ratingX[i],))+", "+str((dolzineX[i],))+", "+str(summaryX[i])+"\n")
#                k+=1

#Vse filme zapišem v eno datoteko brez ponavljanja. Format zapisa je enak kot zgoraj.
        with open("seznamNaslovov.txt","a",encoding="utf-8") as f:
            if os.path.isfile("seznamNaslovov.txt"): #Prepreči ponovno zapisovanje v datoteko, če program poženemo večkrat.
                continue
            else:
                for i in range(50):
                    (naslov1,leto1)=nasloviX[i][1].split("(")
                    (naslov,leto) = (naslov1[:len(naslov1)-1],leto1[:len(leto1)-1])
                    if naslov not in vsiNaslovi:            
                        f.write(nasloviX[i][0]+"| "+naslov+"| "+leto+"| "+str(reziserX[i])+"| "+str(castX[i])+"| "+str(zanriX[i])+"| "+str((ratingX[i],))+"| "+str((dolzineX[i],))+"| "+str(summaryX[i])+"\n")
                        vsiNaslovi.append(naslov)
                        
                    
        for i in castX:
            for j in range(len(i)):
                vsiIgralci.add(i[j])

        for i in reziserX:
            vsiReziserji.add(i)

#Ustvarim še datoteki s seznamoma vseh igralcev/režiserjev.
with open("reziserji.txt","w",encoding="utf-8") as f:
    for i in vsiReziserji:
        f.write(i[0]+", "+i[1]+"\n")


with open("igralci.txt","w",encoding="utf-8") as f:
    for i in vsiIgralci:
        f.write(i[0]+", "+i[1]+"\n")
        
